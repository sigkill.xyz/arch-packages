# Arch Packages
Repo contains modified PKGBUILD files for official/AUR Arch Linux packages. There are two branches:
- `original`: contains unmodified PKGBUILD files.
- `master`: forked from `original` branch, contains customizations.

## Adding/updating a package
- `git checkout original`
- Get build files (PKGBUILD, etc): `yaourt -G <package>`
- Add/commit new files
- `git checkout master && git merge original`

## Customize a package
- Ensure `original` branch is up to date and merged into `master`
- Make changes in `master` branch
- Build & install package with `makepkg -scfi`

## Package Details
### protobuf
Using older-than-latest version.
### st
Simple Terminal configs are done at compile-time by modifying a C header file. Extra functionality added via official patches: http://st.suckless.org/patches/
